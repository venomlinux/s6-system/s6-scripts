V=0.4

PREFIX ?= /usr
SYSCONFDIR = /etc
RUNDIR = /run
BINDIR = $(PREFIX)/bin
S6DIR = $(SYSCONFDIR)/s6

CONTRIB = script/s6-db-reload script/s6-service script/s6-status
DBCONF = data/s6-db-reload.conf

DMODE = -dm0755
EMODE = -m0755
MODE = -m0644
RM = rm -f
M4 = m4 -P --define=m4_s6_contrib_version=$V
CHMODAW = chmod a-w
CHMODX = chmod +x

all: $(CONTRIB)

EDIT = sed \
	-e "s|@sysconfdir[@]|$(DESTDIR)$(SYSCONFDIR)|g" \
	-e "s|@rundir[@]|$(DESTDIR)$(RUNDIR)|g"

%: %.in Makefile
	@echo "GEN $@"
	@$(RM) "$@"
	@{ echo -n 'm4_changequote([[[,]]])'; cat $@.in; } | $(M4) | $(EDIT) >$@
	@$(CHMODAW) "$@"
	@$(CHMODX) "$@"
	@bash -O extglob -n "$@"

clean:
	rm -f $(CONTRIB)

install:
	install $(DMODE) $(DESTDIR)$(S6DIR)
	install $(MODE) $(DBCONF) $(DESTDIR)$(S6DIR)

	install $(DMODE) $(DESTDIR)$(BINDIR)
	install $(EMODE) $(CONTRIB) $(DESTDIR)$(BINDIR)

.PHONY: install
